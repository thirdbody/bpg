use crate::helpers;

pub fn list() {
    helpers::shell::cmd("gpg", "--list-keys");
}

pub fn create() {
    helpers::shell::cmd("gpg", "--gen-key");
}

pub fn import(path: String) {
    helpers::shell::cmd("gpg", &format!("--import {}", &path));
}

pub fn export(key_id: String) {
    helpers::shell::cmd("gpg", &format!("--export -a {}", &key_id));
}

pub fn generate_revocation(key_id: String) {
    helpers::shell::cmd(
        "gpg",
        &format!(
            "--output revocation-{}.asc --gen-revoke {}",
            &key_id, &key_id
        ),
    );
    println!();
    let opt: String = helpers::user_input::get_user_input(
        "Revocation certificate generated to revocation.asc. Revoke key right now? Y/N",
    );
    if &opt == "Y" || &opt == "y" {
        revoke(String::from(format!("revocation-{}.asc", &key_id)));
    } else {
        println!("Not revoking keys.");
    }
}

pub fn revoke(cert: String) {
    helpers::shell::cmd("gpg", &format!("--import {}", cert));
}

pub fn edit(key_id: String) {
    helpers::shell::cmd("gpg", &format!("--edit-key {}", key_id));
}
