use crate::helpers;
use std::fs::remove_file;

pub fn encrypt_message(msg: String, recipients: Vec<String>) {
    let file_path: &str = &format!("bgp-{}", helpers::epoch_time());
    helpers::filesystem::make_and_write_file(String::from(file_path), msg);

    let mut recipient_arg = String::new();
    for recipient in recipients {
        recipient_arg.push_str(&format!("-r {} ", recipient));
    }
    recipient_arg.pop();

    helpers::shell::cmd(
        "gpg",
        &format!("--encrypt --sign -a {} {}", recipient_arg, file_path),
    );
    remove_file(file_path).expect(&format!("Couldn't remove temporary file at {}!", file_path));

    println!(
        "{}",
        &helpers::filesystem::read_file(format!("{}.asc", file_path))
    );

    remove_file(format!("{}.asc", file_path))
        .expect("Couldn't delete temporary file (containing encrypted message)!");
}

pub fn decrypt_message() {
    let msg = helpers::user_input::get_pgp_msg_input();
    let file_path: &str = &format!("bgp-{}", helpers::epoch_time());

    helpers::filesystem::make_and_write_file(String::from(file_path) + ".asc", msg);
    helpers::shell::cmd("gpg", &format!("{}.asc", file_path));
    remove_file(&(String::from(file_path) + ".asc"))
        .expect("Couldn't remove temp file in current directory!");

    println!(
        "\n\nDecrypted message:\n\n{}",
        &helpers::filesystem::read_file(format!("{}", file_path))
    );
    remove_file(file_path).expect("Couldn't remove temporary decrypted file in current directory!");
}
