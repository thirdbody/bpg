extern crate clap;
use clap::{App, Arg, SubCommand};
use std::process;

mod bpg;
mod helpers;

fn main() {
    let matches = App::new("bpg")
        .version("2.2.1")
        .author("Milo Gilad <myl0gcontact@gmail.com>")
        .about("Simple wrapper around gpg")
        .subcommand(
            SubCommand::with_name("tutorial")
                .about("Basic overview of GPG/BPG and how public/private key crypto works."),
        )
        .subcommand(
            SubCommand::with_name("keys")
                .about("Manages keys in your gpg keychain")
                .arg(
                    Arg::with_name("list")
                        .short("l")
                        .long("list")
                        .help("Lists all currently installed keys")
                        .takes_value(false),
                )
                .arg(
                    Arg::with_name("create")
                        .short("c")
                        .long("create")
                        .help("Generates a new keypair.")
                        .takes_value(false),
                )
                .arg(
                    Arg::with_name("import")
                        .short("i")
                        .long("import")
                        .help("Import a new key from a file.")
                        .value_name("FILE")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("export")
                        .short("e")
                        .long("export")
                        .help("Export a key as an ASCII-armoured block.")
                        .value_name("KEY_ID")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("revoke")
                        .short("r")
                        .long("revoke")
                        .help("Generates a revocation cert for (and optionally imports) a key.")
                        .value_name("KEY_ID")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("edit")
                        .long("edit")
                        .help("Pulls up the interactive key-editing menu from GPG.")
                        .value_name("KEY_ID")
                        .takes_value(true),
                ),
        )
        .subcommand(
            SubCommand::with_name("encrypt")
                .about("Encrypts a message")
                .arg(
                    Arg::with_name("recipients")
                        .short("r")
                        .long("recipients")
                        .help("Space-separated list of recipients for the message.")
                        .value_name("EMAILS")
                        .takes_value(true)
                        .required(false),
                )
                .arg(
                    Arg::with_name("message")
                        .short("m")
                        .long("message")
                        .help("The message to encrypt.")
                        .value_name("TEXT")
                        .takes_value(true)
                        .required(false),
                ),
        )
        .subcommand(SubCommand::with_name("decrypt").about("Decrypts a message"))
        .get_matches();

    match matches.subcommand() {
        ("keys", Some(sub_m)) => {
            if sub_m.is_present("list") {
                bpg::keys::list();
                process::exit(0);
            } else if sub_m.is_present("create") {
                bpg::keys::create();
                process::exit(0);
            } else if sub_m.is_present("import") {
                bpg::keys::import(String::from(sub_m.value_of("import").unwrap()));
                process::exit(0);
            } else if sub_m.is_present("export") {
                bpg::keys::export(String::from(sub_m.value_of("export").unwrap()));
                process::exit(0);
            } else if sub_m.is_present("revoke") {
                bpg::keys::generate_revocation(String::from(sub_m.value_of("revoke").unwrap()));
                process::exit(0);
            } else if sub_m.is_present("edit") {
                bpg::keys::edit(String::from(sub_m.value_of("edit").unwrap()));
                process::exit(0);
            } else {
                println!("Wrong usage! See bpg keys -h for more info.");
                process::exit(1);
            }
        }
        ("encrypt", Some(sub_m)) => {
            let mut recipients: String;

            if sub_m.is_present("recipients") {
                recipients = String::from(sub_m.value_of("recipients").unwrap());
            } else {
                recipients =
                    helpers::user_input::get_user_input("Enter recipient emails, space-separated");
            }

            let message: String;
            if sub_m.is_present("message") {
                message = String::from(sub_m.value_of("message").unwrap());
            } else {
                message = helpers::user_input::get_user_input("Go ahead and type your message");
            }

            bpg::encryption::encrypt_message(
                message,
                helpers::to_vec(recipients),
            );
            process::exit(0);
        }
        ("decrypt", Some(_sub_m)) => {
            bpg::encryption::decrypt_message();
        }
        ("tutorial", Some(_sub_m)) => {
            let help = r#"
BPG stands for Better Privacy Guard, and is a nicer user-interface for GPG.

GPG is an implementation of the PGP (Pretty Good Privacy) protocol, which is outlined below.

Imagine Alice wants to send a message to Bobert. However, Alice needs to send this message to Bob over an insecure connection (such as email, which is not encrypted).

Alice can encrypt her message to Bob, sure, but how does she make sure Bob is the only person to read that message? She could use a password, but passwords can be brute-forced.

Instead, Alice and Bob both generate a PGP keypair. A keypair is comprised of a public and private key, where a public key is shared with the whole wide world and the private key better not be shared with anyone!

Alice, upon obtaining Bob's public key, can use her private key to encrypt a message to Bob that only his private key can decrypt, thus ensuring that the message Alice intends for Bob is only ever read by Bob.
            "#;

            println!("{}", help);
        }
        _ => {
            println!("Invalid usage! Run bpg help for more info.");
            process::exit(1);
        }
    }
}
