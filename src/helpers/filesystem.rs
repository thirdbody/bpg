use std::fs::File;
use std::io::prelude::*;

pub fn make_and_write_file(path: String, content: String) {
    let mut file = File::create(path)
        .expect("Couldn't create file in CWD! Are permissions correct?");
    file.write_all(content.as_bytes())
        .expect("Failed to write to file!");
}

pub fn read_file(path: String) -> String {
    let mut file = File::open(path)
        .expect("Couldn't open file!");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Couldn't read file!");

    return contents;

}
