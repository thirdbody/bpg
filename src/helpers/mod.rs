pub mod shell;
pub mod user_input;
pub mod filesystem;

use std::time::SystemTime;

pub fn to_vec(space_delimited: String) -> Vec<String> {
    return space_delimited.split_terminator(" ").map(|s| s.to_string()).collect();
}

pub fn epoch_time() -> String {
    match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
        Ok(n) => return n.as_secs().to_string(),
        Err(_) => panic!("SystemTime before UNIX EPOCH!"),
    }
}